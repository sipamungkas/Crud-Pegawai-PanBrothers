<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Admin\Employee;

class AdminController extends Controller
{
    //

    public function index()
    {
       
        $l = Employee::where('jenis_kelamin','l')->count();
        $p = Employee::where('jenis_kelamin','p')->count();
        $ageR = DB::table('employees')->select(DB::raw("
        case
          when datediff(now(), tanggal_lahir) / 365.25 > 50 then '51+'
          when datediff(now(), tanggal_lahir) / 365.25 > 30 then '31-50'                                     
          when datediff(now(), tanggal_lahir) / 365.25 > 19 then '10-30'                                     
          else '1-20'
      end as age_group
      "))->get();
      $a = 0;
      $b = 0;
      $c = 0;
      $d=0;
      foreach ($ageR as $key) {
          if($key->age_group=='51+'){
              $a++;
          }elseif ($key->age_group=='31-50') {
              $b++;
          }elseif ($key->age_group=='10-30') {
              $c++;
          }else{
              $d++;
          }
      }
        return view('admin.index')->with(['l'=>$l,'p'=>$p,'a'=>$a,'b'=>$b,'c'=>$c,'d'=>$d]);
    }

    public function pegawai()
    {
        # code...
        $employees = Employee::all();
        
        return view('admin.pegawai.index')->with('employees',$employees);
    }

    public function tambah()
    {
        # code...
        return view('admin.pegawai.tambah');
    }

    public function edit($id)
    {
        # code...
        $employee = Employee::findOrFail($id);
        return view('admin.pegawai.edit')->with('employee',$employee);
    }
    public function store(Request $request)
    {
        # code...
        $pegawai = new Employee;
        $pegawai->nama = $request->nama;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = $request->tanggal_lahir;
        $pegawai->jenis_kelamin = $request->jk;
        $pegawai->alamat = $request->alamat;
        $pegawai->save();
        return redirect()->route('admin.pegawai.index');
    }

    public function delete($id)
    {
        
            $em = Employee::findOrFail($id);
            $em->delete();
            return redirect()->route('admin.pegawai.index');
    }

    public function update($id, Request $request){
        $pegawai = Employee::findOrFail($id);
        $pegawai->nama = $request->nama;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = $request->tanggal_lahir;
        $pegawai->jenis_kelamin = $request->jk;
        $pegawai->alamat = $request->alamat;
        $pegawai->save();
        return redirect()->route('admin.pegawai.index');
    }
}
