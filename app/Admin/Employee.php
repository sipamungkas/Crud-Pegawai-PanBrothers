<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Employee extends Model
{
    //
    protected $fillable = ['nama', 'ttl','alamat','jenis_kelamin','usia'];
    public function getAge()
        {
            return Carbon::parse($this->attributes['tanggal_lahir'])->age;
        }
}

