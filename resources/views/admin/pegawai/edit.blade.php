@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 col-sm-12 col-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">
                                Pegawai
                            </h3>
                            {{-- <a class="btn btn-info btn-sm float-right" href="{{route('admin.user.tambah')}}"> +
                            Tambah</a> --}}
                            <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal"
                                data-target="#modal-tambah"> + Tambah</button>
                        </div>

                    </div><!-- /.card-header -->
                    <div class="card-body">

                        <form method="post" action="{{route('admin.pegawai.update',$employee->id)}}">
                            @csrf
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="Nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Pegawai"
                                    value="{{$employee->nama}}">
                            </div>
                            <div class="form-group">
                                <label for="tptLahir">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir"
                                    placeholder="Tempat Lahir" value="{{$employee->tempat_lahir}}">
                            </div>
                            <div class="form-group">
                                <label for="tglLahir">Tanggal Lahir</label>
                                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir"
                                    placeholder="1970-06-19" value="{{$employee->tanggal_lahir}}"></div>

                            <div class="form-group">
                                <label for="jk">Jenis Kelamin</label>
                                <select name="jk" class="custom-select">
                                    <option value="l">Laki-laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat"
                                    value="{{$employee->alamat}}">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                <!-- /.card -->

            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>


@endsection