@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 col-sm-12 col-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">
                                Pegawai
                            </h3>
                            <a class="btn btn-info btn-sm float-right" href="{{route('admin.pegawai.tambah')}}"> +
                                Tambah</a>
                            {{-- <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal"
                                data-target="#modal-tambah"> + Tambah</button> --}}
                        </div>

                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                            aria-describedby="example2_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-sort="ascending" aria-label="#">#</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Nama">
                                        Nama</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Usia">
                                        TTL</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Usia">
                                        Usia</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Jenis Kelamin">
                                        Jenis Kelamin</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Alamat">
                                        Alamat</th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Action">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)

                                <tr role="row">
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$employee->nama}}</td>
                                    <td>{{$employee->tempat_lahir.', '.tgl_indo($employee->tanggal_lahir)}}
                                    </td>
                                    <td>{{$employee->getAge()}}</td>
                                    <td>{{$employee->jenis_kelamin == 'l' ? "Laki-laki" : "Perempuan"}}</td>
                                    <td>{{$employee->alamat}}</td>
                                    <td><a class="btn btn-warning btn-sm"
                                            href="{{route('admin.pegawai.edit',$employee->id)}}">Edit</a>
                                        <a href="{{route('admin.pegawai.delete',$employee->id)}}"
                                            class="btn btn-danger btn-sm">Hapus</a></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->

            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<?php
function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>
{{-- @include('admin.pegawai.modal_tambah') --}}
<!-- /.content -->
@endsection
@section('script')
<script>
    $(function () {
    $("#example2").DataTable();
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

  function fetchData() {
    $.ajax({
        url: "",
        method: "get",
        success: function(res){
          console.log(res);
        }
      })
  }
  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

</script>
@endsection