@extends('layouts.admin')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Pegawai</a></li>
                    <li class="breadcrumb-item active">Tambah</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 col-sm-12 col-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">
                                Pegawai
                            </h3>
                            {{-- <a class="btn btn-info btn-sm float-right" href="{{route('admin.user.tambah')}}"> +
                            Tambah</a> --}}
                            <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal"
                                data-target="#modal-tambah"> + Tambah</button>
                        </div>

                    </div><!-- /.card-header -->
                    <div class="card-body">

                        <form method="post" action="{{route('admin.pegawai.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="Nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama"
                                    placeholder="Nama Pegawai">
                            </div>
                            <div class="form-group">
                                <label for="tptLahir">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir"
                                    placeholder="Tempat Lahir" onblur="tgl()">
                            </div>
                            <div class="form-group">
                                <label for="tglLahir">Tanggal Lahir</label>
                                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir"
                                    placeholder="1970-06-19" onchange="hitUsia()" </div> <div class="form-group">
                                <label for="Nama">Usia</label>
                                <input type="int" name="usia" class="form-control" id="usia" placeholder="Usia Anda">
                            </div>
                            <div class="form-group">
                                <label for="jk">Jenis Kelamin</label>
                                <select name="jk" class="custom-select">
                                    <option value="l">Laki-laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                <!-- /.card -->

            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>

<script>
    function hitUsia() {  
        var dob = $('#tanggal_lahir').val();
        dob = new Date(dob);
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        console.log(age);
        $('#usia').val(age+' Tahun');
    }
</script>

@endsection