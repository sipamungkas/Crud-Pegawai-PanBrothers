<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	// for($i = 1; $i <1; $i++){
 
            // insert data ke table pegawai menggunakan Faker
          DB::table('employees')->insert([
              'nama' => $faker->name,
              'tempat_lahir' => array_random(['Jepara','Kudus','Semarang']),
              'jenis_kelamin' => array_random(['l','p']),
              'tanggal_lahir' => $faker->dateTimeThisCentury->format('Y-m-d'),
              'alamat' => $faker->address,
          ]);
    // }
}
}