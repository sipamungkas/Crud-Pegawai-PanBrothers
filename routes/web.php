<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('auth')->group(function(){
    Route::get('/','AdminController@index')->name('index');
    Route::get('/pegawai','AdminController@pegawai')->name('pegawai.index');
    Route::get('/pegawai/tambah','AdminController@tambah')->name('pegawai.tambah');
    Route::post('pegawai/tambah','AdminController@store')->name('pegawai.store');
    Route::post('pegawai/tambah','AdminController@store')->name('pegawai.store');
    Route::put('/pegawai/{id}/update', 'AdminController@update')->name('pegawai.update');
    Route::get('/pegawai/{id}/edit','AdminController@edit')->name('pegawai.edit');
    Route::get('/pegawai/{id}/delete','AdminController@delete')->name('pegawai.delete');
});